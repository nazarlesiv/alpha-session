<?php
namespace Alpha\Session;

/*
 *Session class
 */

class Session {

  private $_branch = null;

  /*
   *@method __constructor () - constructor method for the session object.
   */
  public function __construct() {

    $this->_branch = &$_SESSION;
  }

  /*
   *@method getSessionId() - Get the session Id.
   *@return Mixed - Returns a session id as String, or false on failure.
   */
  public function getSessionId() {
    return session_id();
  }

  /*
   *@method setSessionId($id) - Set the session Id.
   *@return Mixed - Returns a session id as String, or false on failure.
   */
  public function setSessionId($id) {

    if(!is_string($id)) { return false; }
    return session_id($id);
  }

  /*
   * @method exists() - Checks of the session is started.
   * @return Bool - REturn true if session is started, false otherwise.
   */
  public function exists() {
    return session_status() == PHP_SESSION_ACTIVE;
  }

  public function enabled() {
    return !(session_status() == PHP_SESSION_DISABLED);
  }

  public function get($key = null) {

    //if key is null, return the whole branch
    if(is_null($key) || empty($key)) {
      return $this->_branch;
    }

    //if the key is not a string return null.
    if(!is_string($key)) { return null; }

    //split the key on / to drill down for grouped request data.
    $parts = $this->parse($key);

    //temporary container.
    //set initialy to the $_REQUEST array.
    $temp = $this->_branch;

    //drill down the grouped values in the request array.
    foreach($parts as $key) {

      //check if the key exists in the temp array.
      //if so assign that value to temp.
      if(isset($temp[$key])) {
        $temp = $temp[$key];
      } else {

        //if the key does not exists inside the temp container,
        //return null.
        return null;
      }
    }

    //return the temp value.
    return $temp;
  }

  public function set($key, $data) {
    //if key is null, return the whole registry
    if(!is_string($key) || empty($key)) {
      return $this;
    }

    $parts =  $this->parse($key);

    $temp = &$this->_branch;

    //loop through each part of the key.
    for($i = 0; $i < count($parts)-1; $i++) {
      /*if the key is not set inside the array.
       *create an empty array, and set it as a
       *value for that $key
       */

      if(!isset($temp[$parts[$i]])) {

        $temp[$parts[$i]] = array();
        //$temp[$key] = array();

        //set temp to point to the newly created array.
        $temp = &$temp[$parts[$i]];
        //$temp = $temp[$key];

      } else if(isset($temp[$parts[$i]])) {
        /*if the key is set inside the temp array.
         *set $temp equal to that element.
         */
        $temp = &$temp[$parts[$i]];

      }
    }
    //set the data to the last key.
    $temp[$parts[count($parts)-1]] = $data;

    return true;
  }

  /*
   *@method destroy() - Destroy the current session.
   *@param String $key - Name of the particular session branch to destroy.
   *@return Boolean - True on success, false otherwise.
   */
  public function destroy($key = null) {
    /*
     *In order to kill the session altogether, like to log the user out,
     *the session id must also be unset. If a cookie is used to propagate
     *the session id (default behavior), then the session cookie must be
     *deleted. setcookie() may be used for that.
     */

    //if the branch is not pulled, destroy the whole session.
    if(is_null($key) || !is_string($key)) {

      //unset the session id.

      //unes the session variables.
      session_unset();

      //destroy the session.
      return session_destroy();

    } elseif (!is_null($key) && is_string($key)) {

      //unset the specified session branch is exists.
      //split the key on / to drill down for grouped request data.
      $parts = explode('/', $key);

      //$temp = &$_SESSION;
      $temp = &$this->_branch;

      //loop through each part of the key.
      for($i = 0; $i < count($parts)-1; $i++) {

        /*
         *if the key is not set inside the array.
         *return false
         */

        if(!isset($temp[$parts[$i]])) {



          //return false
          return false;

        } else if(isset($temp[$parts[$i]])) {

          /*
           *if the key is set inside the temp array.
           *set $temp equal to that element.
           */

          $temp = &$temp[$parts[$i]];

        }
      }

      //check if the last key is set inside the temp, if so, unset it.
      if(isset($temp[$parts[count($parts)-1]])) {
        unset($temp[$parts[count($parts)-1]]);
        //return true
        return true;
      }


      //return false otherwise.
      return false;
    }
  }

  /*
   *@method unset() - Unset all the elements from the session.
   */
  public function unsetSession() {
    session_unset();
  }

  /*
   * @method parse() - parses the path string into an array of nodes.
   * @param String $path - String path delimited by a . or a /
   */
  public function parse($path) {
    if(!is_string($path) || empty($path)) { return array(); }
    return preg_split('/[\.\/]/', $path);
  }

}
